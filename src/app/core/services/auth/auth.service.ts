import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // API URL
  private api = environment.apiUrl;

  constructor(
    private _http: HttpClient
  ) { }

  logIn(credentials: any) {
    return this._http.post(`${this.api}/AppUsers/login?include=user`, credentials);
  }
}
