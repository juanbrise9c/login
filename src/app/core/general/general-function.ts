import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({ providedIn: 'root' })
export class GeneralFunctions {
  constructor(
    private _snackbar: MatSnackBar
  ) { }

  createToast(message: string) {
    this._snackbar.open(message, undefined, {
      duration: 3000,
      horizontalPosition: 'end',
      verticalPosition: 'top'
    });
  }
}
