import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { GeneralFunctions } from '../../core/general/general-function';
import { AuthService } from '../../core/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private _fb: FormBuilder,
    private _authService: AuthService,
    private _generalFunctions: GeneralFunctions
  ) { }

  addForm = this._fb.group({
    username: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required),
  });

  ngOnInit(): void {
  }

  logIn(): void {
    this._authService.logIn(this.addForm.value).subscribe(
      (val: any) => { 
        const dataEncrypted = btoa(JSON.stringify(val));
        localStorage.setItem('user_token', dataEncrypted);
        this._generalFunctions.createToast('Usuario ha sido loggeado exitosamente.');
      },
      (err: any) => { 
        console.log('Error', err); 
        this._generalFunctions.createToast('Usuario no ha sido loggeado exitosamente.');
      }
    );
  }
}
